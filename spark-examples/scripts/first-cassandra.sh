#!/usr/bin/env bash

$SPARK_HOME/bin/spark-submit \
  --verbose \
  --class spark.example.cassandra.FirstCassandra \
  --master spark://192.168.31.114:7077 \
  --executor-memory 7G \
  ../target/scala-2.11/spark-mongodb.jar
#  --deploy-mode cluster \
