#!/usr/bin/env bash

$SPARK_HOME/bin/spark-submit \
  --verbose \
  --class spark.example.mongodb.Mini \
  --master spark://192.168.31.116:7077 \
  --executor-memory 6G \
  --driver-memory 6G \
  ../target/scala-2.11/spark-mongodb.jar
#  --deploy-mode cluster \
