#!/usr/bin/env bash

nohup $SPARK_HOME/bin/spark-submit \
  --class spark.example.qq.QQNicknames \
  --master spark://192.168.31.116:7077 \
  --name "QQData" \
  --executor-memory 15G \
  --total-executor-cores 8 \
  --driver-memory 10G \
  --conf spark.serializer=org.apache.spark.serializer.KryoSerializer \
  --conf spark.akka.frameSize=1024 \
  --conf "spark.executor.extraJavaOptions=-XX:+PrintGCDetails -XX:+PrintGCTimeStamps" \
  --conf spark.shuffle.consolidateFiles=true \
  ~/spark-example.jar &
