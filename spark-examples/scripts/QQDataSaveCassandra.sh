#!/usr/bin/env bash

nohup $SPARK_HOME/bin/spark-submit \
  --class spark.example.qq.QQDataSaveCassandra \
  --master spark://192.168.31.116:7077 \
  --name "QQData" \
  --executor-memory 5G \
  --executor-cores 3 \
  --total-executor-cores 24 \
  --driver-memory 15G \
  --conf spark.serializer=org.apache.spark.serializer.KryoSerializer \
  --conf spark.akka.frameSize=1024 \
  --conf "spark.executor.extraJavaOptions=-XX:+PrintGCDetails -XX:+PrintGCTimeStamps" \
  --conf spark.shuffle.consolidateFiles=true \
  ~/spark-example.jar &
