package spark.example.cassandra

import com.datastax.spark.connector._
import org.apache.spark.{SparkConf, SparkContext}

/**
 * First Cassandra
 * Created by Yang Jing (yangbajing@gmail.com) on 2015-10-20.
 */
class FirstCassandra(sc: SparkContext) {

  def apply(): Unit = {
    val groupsRDD = sc.parallelize(Seq(
      ("t418775911", Seq("234232324", "9283423")),
      ("t418775910", Seq("23423232", "9283423")),
      ("t418775912", Seq("23423232", "9283423"))
    ))

    val nicksRDD = sc.parallelize(Seq(
      ("t418775912", Seq("百八井", "枯叶草 flsk")),
      ("t418775911", Seq("全哈", "要呆呆")),
      ("t418775910", Seq("羊八井", "杨家将")),
      ("t418775920", Seq("羊八井", "29要不", "要呆呆"))
    ))

    groupsRDD.saveToCassandra("scspace", "qq_infos", SomeColumns("qq", "groups".append))
    nicksRDD.saveToCassandra("scspace", "qq_infos", SomeColumns("qq", "nicks".append))

    val rdd = sc.cassandraTable("scspace", "qq_infos")
    rdd.foreach(println)
    sc.stop()
  }

}

object FirstCassandra {
  def apply(args: Array[String]) {
    val conf = new SparkConf()
      .set("spark.cassandra.connection.host", "192.168.31.116")
    val sc = new SparkContext(conf)
    val fc = new FirstCassandra(sc)
    fc()
  }
}
