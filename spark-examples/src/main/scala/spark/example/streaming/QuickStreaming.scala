package spark.example.streaming

import org.apache.spark.{HashPartitioner, SparkConf}
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
 * Created by Yang Jing (yangbajing@gmail.com) on 2015-10-22.
 */
class QuickStreaming(conf: SparkConf) {

  def apply() {
    val updateFunc = (values: Seq[Int], state: Option[Int]) => {
      Some(values.sum + state.getOrElse(0))
    }

    val newUpdateFunc = (iter: Iterator[(String, Seq[Int], Option[Int])]) => {
      iter.flatMap(t => updateFunc(t._2, t._3).map(s => (t._1, s)))
    }

    val ssc = new StreamingContext(conf, Seconds(5))
    ssc.checkpoint("/tmp/")

    val initialRDD = ssc.sparkContext.parallelize(Seq(("hello", 1), ("word", 1)))

    val lines = ssc.socketTextStream("localhost", 39999)

    val words = lines.flatMap(_.split(" "))
    val pairs = words.map(word => (word, 1))

    val state = pairs.updateStateByKey[Int](newUpdateFunc,
      new HashPartitioner(ssc.sparkContext.defaultParallelism), true, initialRDD)

    state.print()

    ssc.start()
    ssc.awaitTermination()
  }
}

object QuickStreaming {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
    val qs = new QuickStreaming(conf)
    qs()
  }
}
