package spark.example.qq

import com.mongodb.BasicDBObject
import com.mongodb.hadoop.{MongoInputFormat, MongoOutputFormat}
import org.apache.hadoop.conf.Configuration
import org.apache.spark.{SparkConf, SparkContext}
import org.bson.BSONObject
import spark.example.qq.Utils._

import scala.collection.JavaConverters._

/**
 * Connect MongoDB
 * Created by Yang Jing (yangbajing@gmail.com) on 2015-09-12.
 */
class QQNicknames(sc: SparkContext) {

  def run(): Unit = {
    //    val sparkConf = new SparkConf().setAppName("Mini")
    //    val sc = new SparkContext(sparkConf)

    val inputConfig = new Configuration()
    inputConfig.set("mongo.input.uri", "mongodb://192.168.31.121:27017/qq_db.qqInfo")
    inputConfig.set("mongo.input.fields", """{"qq":1, "nickname":1, "_id":0}""")
    inputConfig.set("mongo.input.noTimeout", "true")

    val documentRDD = sc.newAPIHadoopRDD(
      inputConfig,
      classOf[MongoInputFormat],
      classOf[Object],
      classOf[BSONObject])

    val qqRDD = documentRDD.map { case (_, doc) =>
      getValue(doc, "qq") -> Set(getValue(doc, "nickname"))
    }

    val resultRDD = qqRDD.reduceByKey { (x, y) =>
      x ++ y
    }.map { case (qq, nicknames) =>
      val o = new BasicDBObject()
      o.put("nicknames", nicknames.asJava)
      (qq, o)
    }

    val outputConfig = new Configuration()
    outputConfig.set("mongo.output.uri", "mongodb://192.168.31.114:27017/qq_db_result.qqInfo")

    resultRDD.saveAsNewAPIHadoopFile(
      "file://this-is-completely-unused",
      classOf[Object],
      classOf[BSONObject],
      classOf[MongoOutputFormat[Object, BSONObject]],
      outputConfig)
  }

}

object QQNicknames {
  def main(args: Array[String]): Unit = {
    val sparkConf = new SparkConf()
    val sc = new SparkContext(sparkConf)
    val qg = new QQNicknames(sc)
    qg.run()
  }
}
