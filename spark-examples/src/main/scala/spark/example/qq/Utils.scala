package spark.example.qq

import org.bson.BSONObject

/**
 * Utils
 * Created by Yang Jing (yangbajing@gmail.com) on 2015-10-27.
 */
object Utils {

  def getValue(dbo: BSONObject, key: String): String = {
    val value = dbo.get(key)
    if (value eq null) "" else value.asInstanceOf[String]
  }

}
