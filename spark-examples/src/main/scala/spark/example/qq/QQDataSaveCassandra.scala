package spark.example.qq

import com.datastax.spark.connector._
import com.mongodb.hadoop.MongoInputFormat
import org.apache.hadoop.conf.Configuration
import org.apache.spark.{SparkConf, SparkContext}
import org.bson.BSONObject
import spark.example.qq.Utils._

/**
 * QQ数据测试，计算每个QQ ID加入的QQ群，结果存于 qq_db_result.qqInfo
 *
 * Cassandra create cql:
 * create keyspace scspace with replication = {'class': 'SimpleStrategy', 'replication_factor': 2};
 * use scspace;
 * create table qq_infos (
     qq varchar primary key,
     groups list<varchar>,
     nicks list<varchar>
   );
 *
 * Created by Yang Jing (yangbajing@gmail.com) on 2015-09-18.
 */
object QQDataSaveCassandra {
  def main(args: Array[String]): Unit = {
    val sparkConf = new SparkConf()
      .set("spark.cassandra.connection.host", "192.168.31.101")
      .set("spark.cassandra.connection.port", "9042")

    // 输入配置，使用mongodb-hadoop代理连接器
    val inputConfig = new Configuration()
    inputConfig.set("mongo.input.uri", "mongodb://192.168.31.121:27017/qq_db.qqInfo")
    inputConfig.set("mongo.input.fields", """{"qq":1, "groupId":1, "nickname":1, "_id":0}""")
    inputConfig.set("mongo.input.noTimeout", "true")

    val sc = new SparkContext(sparkConf)

    // 创建Mongodb document RDD
    val documentRDD = sc.newAPIHadoopRDD(
      inputConfig,
      classOf[MongoInputFormat],
      classOf[Object],
      classOf[BSONObject])

    // qq, groupId, nickname
    val qqRDD = documentRDD.map { case (_, doc) =>
      getValue(doc, "qq") -> Tuple2(Set(getValue(doc, "groupId")), Set(getValue(doc, "nickname")))
    }

    val resultsRDD = qqRDD.reduceByKey { (x, y) =>
      (x._1 ++ y._1, x._2 ++ y._2)
    }.map { case (qq, (groups, nicks)) =>
      (qq, groups, nicks)
    }

    resultsRDD.saveToCassandra("scspace", "qq_infos", SomeColumns("qq", "groups", "nicks"))
  }
}