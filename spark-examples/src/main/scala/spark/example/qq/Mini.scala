package spark.example.qq

import com.mongodb.BasicDBObject
import com.mongodb.hadoop.{MongoInputFormat, MongoOutputFormat}
import org.apache.hadoop.conf.Configuration
import org.apache.spark.{SparkConf, SparkContext}
import org.bson.BSONObject

import scala.collection.JavaConverters._

/**
 * Connect MongoDB
 * Created by Yang Jing (yangbajing@gmail.com) on 2015-09-12.
 */
class Mini(sc: SparkContext) {

  def start(): Unit = {
    //    val sparkConf = new SparkConf().setAppName("Mini")
    //    val sc = new SparkContext(sparkConf)

    val inputConfig = new Configuration()
    inputConfig.set("mongo.input.uri", "mongodb://192.168.31.109:27017/test.minibars")
    inputConfig.set("mongo.input.fields", """{"Day":1, "Open":1, "_id":0}""")

    val documentRDD = sc.newAPIHadoopRDD(
      inputConfig,
      classOf[MongoInputFormat],
      classOf[Object],
      classOf[BSONObject])

    val qqRDD = documentRDD.map { case (_, doc) =>
      (
        doc.get("Day").asInstanceOf[Int],
        doc.get("Open") match {
          case i: Integer => i.toDouble
          case d: java.lang.Double => d
        })
    }.map { case (day, open) =>
      (day, Set(open))
    }.reduceByKey(_ ++ _)

    val resultRDD = qqRDD.map { case (day, opens) =>
      val o = new BasicDBObject()
      o.put("day", day.asInstanceOf[Integer])
      o.put("opens", opens.asJava)
      day -> o
    }

    val outputConfig = new Configuration()
    outputConfig.set("mongo.output.uri", "mongodb://192.168.31.109:27017/test_result.dayOpens")

    resultRDD.saveAsNewAPIHadoopFile(
      "file://this-is-completely-unused",
      classOf[Object],
      classOf[BSONObject],
      classOf[MongoOutputFormat[Object, BSONObject]],
      outputConfig)
  }

}
