package spark.example.qq


import java.sql.DriverManager

import com.mongodb.hadoop.MongoInputFormat
import org.apache.hadoop.conf.Configuration
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.{SparkConf, SparkContext}
import org.bson.BSONObject
import org.postgresql.PGConnection
import spark.example.qq.Utils._

import scala.collection.JavaConverters._

/**
 * QQ数据测试，计算每个QQ ID加入的QQ群，结果存于 qq_db_result.qqInfo
 *
 * Cassandra create cql:
 * create keyspace scspace with replication = {'class': 'SimpleStrategy', 'replication_factor': 2};
 * use scspace;
 * create table qq_infos (
     qq varchar primary key,
     groups list<varchar>,
     nicks list<varchar>
   );
 *
 * Created by Yang Jing (yangbajing@gmail.com) on 2015-09-18.
 */
class QQDataSQL {
  /**
   *
   * @param sparkConf
   * @param inputConfig
   * @param options
   * @param columns
   * @param tableName
   */
  def run(sparkConf: SparkConf,
          inputConfig: Configuration,
          options: Map[String, String],
          columns: Seq[String],
          tableName: String): Unit = {

    val sc = new SparkContext(sparkConf)
    //    val sqlCtx = new HiveContext(sc)
    //    import sqlCtx.implicits._

    // 创建Mongodb document RDD
    val documentRDD = sc.newAPIHadoopRDD(
      inputConfig,
      classOf[MongoInputFormat],
      classOf[Object],
      classOf[BSONObject])

    // qq, groupId, nickname
    val qqRDD = documentRDD.map { case (_, doc) =>
      getValue(doc, "qq") -> Tuple2(Set(getValue(doc, "groupId")), Set(getValue(doc, "nickname")))
    }.reduceByKey { (x, y) =>
      (x._1 ++ y._1, x._2 ++ y._2)
    }.map { case (qq, (groups, nicknames)) =>
      (qq, groups, nicknames)
    }

    //    qqRDD.toDF(columns: _*).write.format("jdbc").options(options).saveAsTable(tableName)

    //    qqRDD.saveAsTextFile("/tmp/qq_infos")
    //    qqRDD.saveAsObjectFile("/tmp/qq_infos")

    qqRDD.foreach { case (qq, groups, nicknames) =>
      SQLUtils.saveToSQL(qq, groups, nicknames)
    }

    sc.stop()
    SQLUtils.conn.close()
  }
}

object QQDataSQL {

  def main(args: Array[String]): Unit = {
    Class.forName("org.postgresql.Driver")


    val sparkConf = new SparkConf()

    // 输入配置，使用mongodb-hadoop代理连接器
    val inputConfig = new Configuration()
    inputConfig.set("mongo.input.uri", "mongodb://192.168.31.121:27017/qq_db.qqInfo")
    inputConfig.set("mongo.input.fields", """{"qq":1, "groupId":1, "nickname":1, "_id":0}""")
    inputConfig.set("mongo.input.noTimeout", "true")

    val options = Map(
      "url" -> "jdbc:postgresql://192.168.31.101/scdb",
      "user" -> "scuser",
      "password" -> "scpass")

    val columns = Seq("qq", "groups", "nicknames")

    val tableName = "qq_infos"

    val qds = new QQDataSQL()
    qds.run(sparkConf, inputConfig, options, columns, tableName)
  }
}

object SQLUtils {
  val conn = DriverManager.getConnection("jdbc:postgresql://192.168.31.101/scdb", "scuser", "scpass")

  def saveToSQL(qq: String, groups: Set[String], nicknames: Set[String]): Unit = {
    try {
      val pstmt = conn.prepareStatement("insert into qq_infos(qq, groups, nicknames) values(?, ?, ?)")
      val groupArray = conn.createArrayOf("varchar", groups.toArray)
      val nicknameArray = conn.createArrayOf("varchar", nicknames.toArray)
      pstmt.setString(1, qq)
      pstmt.setArray(2, groupArray)
      pstmt.setArray(3, nicknameArray)
      pstmt.executeUpdate()

      groupArray.free()
      nicknameArray.free()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
  }
}
