package spark.example.qq

import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.WordSpec

/**
 * MiniTest
 * Created by Yang Jing (yangbajing@gmail.com) on 2015-09-22.
 */
class MiniTest extends WordSpec {
  val conf = new SparkConf().setAppName("Mini").setMaster("local[*]")
  val sc = new SparkContext(conf)

  "MiniTest" should {
    "start" in {
      val mini = new Mini(sc)
      mini.start()
    }
  }
}
