package spark.example.qq

import com.datastax.spark.connector._
import com.mongodb.hadoop.MongoInputFormat
import org.apache.hadoop.conf.Configuration
import org.apache.spark.{SparkConf, SparkContext}
import org.bson.BSONObject
import org.scalatest.{BeforeAndAfterAll, MustMatchers, WordSpec}

/**
 * Created by Yang Jing (yangbajing@gmail.com) on 2015-10-25.
 */
class QQDataTest extends WordSpec with MustMatchers with BeforeAndAfterAll {

  val sparkConf = new SparkConf()
    .setMaster("spark://192.168.31.116:7077")
    .setAppName("QQDataTest")
//    .set("spark.executor.memory", "6G")
//    .set("spark.driver.cores", "2")
    .set("spark.cassandra.connection.host", "192.168.31.101")
    .set("spark.cassandra.connection.port", "9042")

  // 输入配置，使用mongodb-hadoop代理连接器
  val inputConfig = new Configuration()
  inputConfig.set("mongo.input.uri", "mongodb://192.168.31.121:27017/qq_db_test.qqInfo")
  inputConfig.set("mongo.input.fields", """{"qq":1, "groupId":1, "nickname":1, "_id":0}""")
  inputConfig.set("mongo.input.noTimeout", "true")

  val sc = new SparkContext(sparkConf)

  "QQDataTest" should {
    "run" in {
      val getValue = (dbo: BSONObject, key: String) => {
        val value = dbo.get(key)
        if (value eq null) "" else value.asInstanceOf[String]
      }

      // 创建Mongodb document RDD
      val documentRDD = sc.newAPIHadoopRDD(
        inputConfig,
        classOf[MongoInputFormat],
        classOf[Object],
        classOf[BSONObject])

      val counter = sc.accumulator(0)

      // qq, groupId, nickname
      val qqRDD = documentRDD.map { case (_, doc) =>
        getValue(doc, "qq") -> Tuple2(Set(getValue(doc, "groupId")), Set(getValue(doc, "nickname")))
      }

      val resultsRDD = qqRDD.reduceByKey { (x, y) =>
        counter += 1
        (x._1 ++ y._1, x._2 ++ y._2)
      }.map { case (qq, (groups, nicks)) =>
        (qq, groups, nicks)
      }

      resultsRDD.saveToCassandra("scspace", "qq_infos", SomeColumns("qq", "groups", "nicks"))
      val count = counter.value
      println("reduceByKey counter: " + count)
      count > 0 mustBe true
    }
  }


  override protected def afterAll(): Unit = {
    sc.stop()
  }
}
