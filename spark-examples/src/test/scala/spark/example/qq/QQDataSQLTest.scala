package spark.example.qq

import org.apache.hadoop.conf.Configuration
import org.apache.spark.SparkConf
import org.scalatest.WordSpec

/**
 * QQDataSQLTest
 * Created by yangjing on 15-10-28.
 */
class QQDataSQLTest extends WordSpec {
  Class.forName("org.postgresql.Driver")

  "QQDataSQLTest" should {

    "run" in {
      val sparkConf = new SparkConf().setAppName("QQDataSQLTest").setMaster("local[2]")

      // 输入配置，使用mongodb-hadoop代理连接器
      val inputConfig = new Configuration()
      inputConfig.set("mongo.input.uri", "mongodb://192.168.31.121:27017/qq_db_test.qqInfo")
      inputConfig.set("mongo.input.fields", """{"qq":1, "groupId":1, "nickname":1, "_id":0}""")
      inputConfig.set("mongo.input.noTimeout", "true")

      val options = Map(
        "url" -> "jdbc:postgresql://192.168.31.101/scdb",
        "user" -> "scuser",
        "password" -> "scpass")

      val columns = Seq("qq", "groups", "nicknames")

      val tableName = "qq_infos"

      val qds = new QQDataSQL()
      qds.run(sparkConf, inputConfig, options, columns, tableName)

    }
  }

}
