package spark.example.cassandra

import org.apache.spark.{SparkContext, SparkConf}
import org.scalatest.{BeforeAndAfterAll, WordSpec}

/**
 * test first cassandra
 * Created by Yang Jing (yangbajing@gmail.com) on 2015-10-20.
 */
class FirstCassandraTest extends WordSpec with BeforeAndAfterAll {
  val conf = new SparkConf()
//    .setMaster("spark://192.168.31.116:7077")
    .setMaster("local")
    .setAppName("FirstCassandra")
    .set("spark.cassandra.connection.host", "192.168.31.101")
    .set("spark.cassandra.connection.port", "9042")
  val sc = new SparkContext(conf)

  "FirstCassandraTest" should {
    "apply" in {
      val fc = new FirstCassandra(sc)
      fc()
    }
  }

  override protected def afterAll(): Unit = {
    sc.stop()
  }
}
