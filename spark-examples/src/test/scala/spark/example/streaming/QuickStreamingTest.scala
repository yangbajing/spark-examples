package spark.example.streaming

import org.apache.spark.SparkConf
import org.scalatest.WordSpec

/**
 * Created by Yang Jing (yangbajing@gmail.com) on 2015-10-22.
 */
class QuickStreamingTest extends WordSpec {
  "QuickStreamingTest" should {
    "apply" in {
      val conf = new SparkConf()
        .setAppName("QuickStreaming")
        .setMaster("local[*]")
      val qs = new QuickStreaming(conf)
      qs()
    }
  }
}
