import _root_.sbt.Keys._
import _root_.sbt._
import com.thoughtworks.sbtApiMappings.ApiMappings
import sbtassembly.AssemblyKeys._
import sbtassembly.{MergeStrategy, PathList}

object Build extends Build {

  override lazy val settings = super.settings :+ {
    shellPrompt := (s => Project.extract(s).currentProject.id + " > ")
  }

  lazy val root = Project("spark-example", file("."))
    .enablePlugins(ApiMappings)
    .settings(basicSettings)
    .settings(
      assemblyJarName in assembly := "spark-example.jar",
      assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false),
      assemblyMergeStrategy in assembly := {
        case PathList("META-INF", "io.netty.versions.properties") => MergeStrategy.discard
        case x =>
          val oldStrategy = (assemblyMergeStrategy in assembly).value
          oldStrategy(x)
      },
      test in assembly := {},
      libraryDependencies ++= Seq(
        _sparkCore,
        _sparkStream,
        _sparkSql,
        _sparkHive,
        //_sparkStreamingKafka,
        _postgresql,
        sparkCassandraConnector,
        _mongoHadoopCore,
        _mongoScala,
        _scalatest)
    )

  val DependsConfigure = "test->test;compile->compile"

  val basicSettings = Seq(
    version := "0.0.1",
    homepage := Some(new URL("http://sc-git/jing.yang/spark-examples.git")),
    organization := "me.yangbajing",
    organizationHomepage := Some(new URL("http://sc-git/jing.yang/spark-examples")),
    startYear := Some(2015),
    scalaVersion := "2.11.7",
    scalacOptions := Seq(
      "-encoding", "utf8",
      "-feature",
      "-unchecked",
      "-deprecation",
      "-explaintypes",
      "-Yno-adapted-args",
      "-Ywarn-dead-code",
      "-Ywarn-unused"
    ),
    javacOptions := Seq(
      "-encoding", "utf8",
      "-deprecation",
      "-Xlint:unchecked",
      "-Xlint:deprecation"
    ),
    offline := true
  )

  private val scopeProvidedTest = "provided,test"
  private val verSpark = "1.5.1"
  private val verHadoop = "2.6.0"
  private val verAkka = "2.3.14"
  private val varAkkaHttp = "1.0"
  private val verMongoHadoop = "1.4.1"
  private val verSparkCassandra = "1.5.0-M2"

  val _sparkCore = "org.apache.spark" %% "spark-core" % verSpark % scopeProvidedTest
  val _sparkSql = "org.apache.spark" %% "spark-sql" % verSpark % scopeProvidedTest
  val _sparkHive = "org.apache.spark" %% "spark-hive" % verSpark % scopeProvidedTest
  val _sparkStream = "org.apache.spark" %% "spark-streaming" % verSpark % scopeProvidedTest
  val _sparkStreamingKafka = "org.apache.spark" %% "spark-streaming-kafka" % verSpark % scopeProvidedTest

  val _hadoopClient = "org.apache.hadoop" % "hadoop-client" % verHadoop % scopeProvidedTest excludeAll ExclusionRule(organization = "javax.servlet")

  val _mongoHadoopCore = "org.mongodb.mongo-hadoop" % "mongo-hadoop-core" % verMongoHadoop excludeAll(
    ExclusionRule(organization = "javax.servlet"), ExclusionRule(organization = "commons-beanutils"), ExclusionRule(organization = "org.apache.hadoop"))

  val sparkCassandraConnector = "com.datastax.spark" %% "spark-cassandra-connector" % verSparkCassandra

  val _akkaActor = "com.typesafe.akka" %% "akka-actor" % verAkka
  val _akkaSlf4j = "com.typesafe.akka" %% "akka-slf4j" % verAkka

  val _akkaHttpCore = "com.typesafe.akka" %% "akka-http-core-experimental" % varAkkaHttp
  val _akkaHttp = "com.typesafe.akka" %% "akka-http-experimental" % varAkkaHttp

  val _postgresql = "org.postgresql" % "postgresql" % "9.4-1201-jdbc41"

  val _playJson = "com.typesafe.play" %% "play-json" % "2.4.3"
  val _mongoScala = "org.mongodb.scala" %% "mongo-scala-driver" % "1.0.0"
  val _slf4j = "org.slf4j" % "slf4j-api" % "1.7.12"
  val _logback = "ch.qos.logback" % "logback-classic" % "1.1.3"
  val _typesafeConfig = "com.typesafe" % "config" % "1.3.0"
  val _scalaLogging = ("com.typesafe.scala-logging" %% "scala-logging" % "3.1.0").exclude("org.scala-lang", "scala-library").exclude("org.scala-lang", "scala-reflect")

  val _scalatest = "org.scalatest" %% "scalatest" % "2.2.5" % "test"

}

