addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.1.0")

addSbtPlugin("com.thoughtworks.sbt-api-mappings" % "sbt-api-mappings" % "0.1.0")

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.0")
