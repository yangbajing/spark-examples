
description := "sbt-example"

version := "0.0.1"

scalaVersion := "2.11.7"

assemblyJarName in assembly := "sbt-example.jar"

assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)

//libraryDependencies ++= Seq(
//  "org.apache.spark" %% "spark-core" % "0.8.0-incubating" % "provided",
//  "org.apache.hadoop" % "hadoop-client" % "2.0.0-cdh4.4.0" % "provided"
//)

